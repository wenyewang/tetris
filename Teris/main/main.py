#!/usr/bin/python3
# -*- coding:utf-8 -*-

'''
Created on 2015��5��5��

@author: Administrator
'''

import sys
import random
import copy
from PyQt5.QtCore import Qt, QBasicTimer, QRect
from PyQt5.QtGui import QColor, QPainter, QBrush, QPen
from PyQt5.QtWidgets import (QWidget, QGridLayout, QHBoxLayout, QVBoxLayout, 
    QPushButton, QApplication, QLabel, QFrame, QLineEdit, QTextEdit)
from copy import deepcopy
from PyQt5.Qt import QVBoxLayout

WIDTH = 15
HEIGHT = 25
BLK_LEN = 20


class Platte(object):
    MAX_COLORS = 16
    def __init__(self):
        self.colors = [QColor(0, 0, 0) for color in range(Platte.MAX_COLORS)]
        
        print(self.colors)
        self.colors[0] = QColor(0, 0, 0)
        
        self.colors[1] = QColor(220, 20, 60)
        self.colors[2] = QColor(255, 0, 255)
        self.colors[3] = QColor(138, 43, 226)

        self.colors[4] = QColor(123, 104, 238)
        self.colors[5] = QColor(0, 0, 240)
        self.colors[6] = QColor(30, 144, 255)
        self.colors[7] = QColor(173, 216, 230)
        
        self.colors[8] = QColor(95, 158, 160)
        self.colors[9] = QColor(0, 255, 209)
        self.colors[10] = QColor(34, 139, 34)
        
        self.colors[11] = QColor(174, 238, 238)
        self.colors[12] = QColor(0, 255, 0)
        self.colors[13] = QColor(255, 255, 0)
        self.colors[14] = QColor(255, 193, 193)
        self.colors[15] = QColor(205, 85, 85)

class Block(object):
    MAX_TYPE = 7
    def __init__(self, type, color, rotateTimes = 0):
        self.state = 0
        self.type = type
        self.color = color
        self.blk = [[ 0 for col in range(2)] for row in range(4)]
        
        if type == 0:
            self.blk[0] = [0, 7]
            self.blk[1] = [0, 8]
            self.blk[2] = [1, 7]
            self.blk[3] = [1, 8]
        
        elif type == 1:
            self.blk[0] = [0, 7]
            self.blk[1] = [1, 7]
            self.blk[2] = [2, 7]
            self.blk[3] = [3, 7]
            
        elif type == 2:
            self.blk[0] = [0, 8]
            self.blk[1] = [1, 8]
            self.blk[2] = [1, 7]
            self.blk[3] = [1, 6]
        
        elif type == 3:
            self.blk[0] = [0, 6]
            self.blk[1] = [1, 6]
            self.blk[2] = [1, 7]
            self.blk[3] = [1, 8]

        elif type == 4:
            self.blk[0] = [0, 8]
            self.blk[1] = [0, 7]
            self.blk[2] = [1, 7]
            self.blk[3] = [1, 6]

        elif type == 5:
            self.blk[0] = [0, 6]
            self.blk[1] = [0, 7]
            self.blk[2] = [1, 7]
            self.blk[3] = [1, 8]            

        elif type == 6:
            self.blk[0] = [0, 7]
            self.blk[1] = [1, 7]
            self.blk[2] = [1, 6]
            self.blk[3] = [1, 8]

        else:
            self.blk[0] = [0, 7]
            self.blk[1] = [0, 8]
            self.blk[2] = [1, 7]
            self.blk[3] = [1, 8]
        
        for i in range(rotateTimes):
            self.rotate()
    
    def rotate(self, position):
        tmpBlk = deepcopy(self.blk)
        if self.type == 0:
            pass
        elif self.type == 1:
            if self.state == 0:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                tmpBlk[3][1] = tmpBlk[3][1] - 2
                
            elif self.state == 1:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                tmpBlk[3][1] = tmpBlk[3][1] + 2
                
            elif self.state == 2:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
                tmpBlk[3][1] = tmpBlk[3][1] + 2

            else:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
                tmpBlk[3][1] = tmpBlk[3][1] - 2

        elif self.type == 2:
            if self.state == 0:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                tmpBlk[3][1] = tmpBlk[3][1] + 2

            elif self.state == 1:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
                tmpBlk[3][1] = tmpBlk[3][1] + 2

            elif self.state == 2:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
                tmpBlk[3][1] = tmpBlk[3][1] - 2
                
            else:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                tmpBlk[3][1] = tmpBlk[3][1] - 2

        elif self.type == 3:
            if self.state == 0:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
                tmpBlk[3][1] = tmpBlk[3][1] - 2

            elif self.state == 1:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                tmpBlk[3][1] = tmpBlk[3][1] - 2

            elif self.state == 2:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                tmpBlk[3][1] = tmpBlk[3][1] + 2
                
            else:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
                tmpBlk[3][1] = tmpBlk[3][1] + 2

        elif self.type == 4:
            if self.state == 0:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                
            elif self.state == 1:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][1] = tmpBlk[3][1] + 2
                
            elif self.state == 2:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2
            
            elif self.state == 3:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][1] = tmpBlk[3][1] - 2

        elif self.type == 5:
            if self.state == 0:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][1] = tmpBlk[3][1] - 2
                
            elif self.state == 1:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] - 2
                
            elif self.state == 2:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][1] = tmpBlk[3][1] + 2
            
            elif self.state == 3:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] + 2            

        elif self.type == 6:
            if self.state == 0:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] + 1
                tmpBlk[3][1] = tmpBlk[3][1] - 1

            elif self.state == 1:
                tmpBlk[0][0] = tmpBlk[0][0] + 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] + 1
                tmpBlk[3][0] = tmpBlk[3][0] - 1
                tmpBlk[3][1] = tmpBlk[3][1] - 1

            elif self.state == 2:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] - 1
                tmpBlk[2][0] = tmpBlk[2][0] + 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] - 1
                tmpBlk[3][1] = tmpBlk[3][1] + 1

            else:
                tmpBlk[0][0] = tmpBlk[0][0] - 1
                tmpBlk[0][1] = tmpBlk[0][1] + 1
                tmpBlk[2][0] = tmpBlk[2][0] - 1
                tmpBlk[2][1] = tmpBlk[2][1] - 1
                tmpBlk[3][0] = tmpBlk[3][0] + 1
                tmpBlk[3][1] = tmpBlk[3][1] + 1

        else:
            pass
        
        if self.isBlkValid(tmpBlk, position) == True:
            self.state = (self.state + 1) % 4
            self.blk = deepcopy(tmpBlk)
    
    def move(self, x, y, position):
        tmpBlk = deepcopy(self.blk)
        for i in range(4):
            tmpBlk[i][0] = tmpBlk[i][0] + x;
            tmpBlk[i][1] = tmpBlk[i][1] + y;
                
        couldMove = self.isBlkValid(tmpBlk, position)
        
#        print(tmpBlk, ';', self.blk)
        if couldMove == True:
            self.blk = deepcopy(tmpBlk)
            return True
        return False

    def isBlkValid(self, blk, position):
        isValid = True
        for i in range(4):
            if blk[i][0] < 0 or blk[i][0] >= HEIGHT:
                isValid = False
            if blk[i][1] < 0 or blk[i][1] >= WIDTH:
                isValid = False
            if isValid == True and position[blk[i][0]][blk[i][1]] != 0:
                isValid = False
        return isValid

class MainWnd(QWidget):
    def __init__(self):
        self.platte = Platte()
        super().__init__()
        self.position = [[0 for col in range(15)] for row in range(25)]
        
        self.timer = QBasicTimer()
        self.int_score = 0
        self.int_time = 0
        self.initUI()
        self.setFixedWidth(300)
        self.setFixedHeight(600)
        self.useBlk = self.newBlk()
        
    def initUI(self):
        self.hbox = QHBoxLayout()
        sclbl = QLabel(self)
        sclbl.setText("Score: ")
        self.score = QLineEdit(self)
        self.score.setFixedWidth(50)
        self.score.setAlignment(Qt.AlignRight)
        self.score.setText("0")
        self.score.setEnabled(False)
        
        timelbl = QLabel(self)
        timelbl.setText("Time: ")
        timelbl.setFixedWidth(50)
        self.time = QLineEdit(self)
        self.time.setText("0")
        self.time.setAlignment(Qt.AlignRight)
        self.time.setEnabled(False)
        self.hbox.addStretch(2)
        self.hbox.addWidget(sclbl)
        self.hbox.addWidget(self.score)
        self.hbox.addStretch(4)
        self.hbox.addWidget(timelbl)
        self.hbox.addWidget(self.time)
        self.hbox.addStretch(2)
        
        self.hbox.setGeometry(QRect(0, 0, 300, 50))
        
        startBtn = QPushButton(self)
        startBtn.setText("Start Game")
        startBtn.clicked.connect(self.startGame)
        pauseBtn = QPushButton(self)
        pauseBtn.setText("Pause Game")
        pauseBtn.clicked.connect(self.pauseGame)
        resetBtn = QPushButton(self)
        resetBtn.setText("Reset Game")
        resetBtn.clicked.connect(self.resetGame)
        self.hbox1 = QHBoxLayout()
        self.hbox1.addWidget(startBtn)
        self.hbox1.addWidget(pauseBtn)
        self.hbox1.addWidget(resetBtn)
        self.hbox1.setGeometry(QRect(0, 550, 300, 50))
        
        self.setWindowTitle("tetris")
        self.show()
    
    def startGame(self):
        self.timer.start(1000, self)
        
    def pauseGame(self):
        self.timer.stop()
    
    def resetGame(self):
        self.timer.stop()
        for i in range(HEIGHT):
            for j in range(WIDTH):
                self.position[i][j] = 0
        self.useBlk = self.newBlk()
        self.score.setText("0")
        self.time.setText("0")
    
    def timerEvent(self, e):
        moveable = True
        if self.useBlk != None:
            moveable = self.useBlk.move(1, 0, self.position)
        
        self.int_time = self.int_time + 1
        if moveable == False:
            solid = self.checkSolid()
            self.checkDisappear()
            if solid == True and self.checkSolid() == True:
                print("Game Over")
                self.resetGame()
        self.repaint()
    
    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        
        self.score.setText(str(self.int_score))
        self.time.setText(str(self.int_time))

        for i in range(HEIGHT):
            for j in range(WIDTH):
                qp.setBrush(self.platte.colors[self.position[i][j]])
                qp.drawRect(j * BLK_LEN, 50 + i * BLK_LEN, BLK_LEN, BLK_LEN)
        
        if self.useBlk != None:
#            print(self.useBlk.color, self.useBlk.blk)
            qp.setBrush(self.platte.colors[self.useBlk.color]);
            for i in range(4):
                qp.drawRect(self.useBlk.blk[i][1] * BLK_LEN, 50 + self.useBlk.blk[i][0] * BLK_LEN, BLK_LEN, BLK_LEN)
        else:
            pass
#            print(self.useBlk.color, self.useBlk.blk)
        qp.end()
    
    def keyPressEvent(self, e):
        moveable = True
        if e.key() == Qt.Key_W or e.key() == Qt.Key_Up:
#            self.lbl.setText('UP')
            self.useBlk.rotate(self.position)
#            self.useBlk.move(-1, 0)
            
        elif e.key() == Qt.Key_A or e.key() == Qt.Key_Left:
#            self.lbl.setText('LEFT')
            moveable = self.useBlk.move(0, -1, self.position)
            
        elif e.key() == Qt.Key_S or e.key() == Qt.Key_Down:
#            self.lbl.setText('DOWN')
            moveable = self.useBlk.move(1, 0, self.position)
            
        elif e.key() == Qt.Key_D or e.key() == Qt.Key_Right:
#            self.lbl.setText('RIGHT')
            moveable = self.useBlk.move(0, 1, self.position)
            
        elif e.key() == Qt.Key_P:
            self.pauseGame()
            
        elif e.key() == Qt.Key_B:
            self.startGame()
        
        if moveable == False:
            solid = self.checkSolid()
            self.checkDisappear()
            if solid == True and self.checkSolid() == True:
                print("Game Over")
                self.resetGame()
        self.repaint()

    def newBlk(self):
        type = random.randint(0, Block.MAX_TYPE - 1)
        color = random.randint(1, Platte.MAX_COLORS - 1)
#        print(type, color)
        return Block(type, color)
    
    def checkSolid(self):
        isSolid = False
        for i in range(4):
            x = self.useBlk.blk[i][0]
            x = x + 1
            y = self.useBlk.blk[i][1]
#            print('x,y :', x - 1, y, ';', self.useBlk.blk)
            
            if x == HEIGHT:
                isSolid = True
                break
            elif self.position[x][y] != 0:
                isSolid = True
                break
        
        if isSolid == True:
            for i in range(4):
                x = self.useBlk.blk[i][0]
                y = self.useBlk.blk[i][1]
                self.position[x][y] = self.useBlk.color
            self.useBlk = self.newBlk()
            return True
        return False
    
    def checkDisappear(self):
        disappearCnt = 0
        x = HEIGHT - 1
        while x > 0:
#        for x in range(HEIGHT - 1, -1, -1):
            cnt = 0
            for y in range(WIDTH):
                if self.position[x][y] != 0:
                    cnt = cnt + 1
            if cnt == WIDTH:
                disappearCnt = disappearCnt + 1
                for tmpX in range(x, 0, -1):
                    for y in range(WIDTH):
                        self.position[tmpX][y] = self.position[tmpX - 1][y]
            else:
                x = x - 1
        
        if disappearCnt == 1:
            self.int_score = self.int_score + 1
        elif disappearCnt == 2:
            self.int_score = self.int_score + 3
        elif disappearCnt == 3:
            self.int_score = self.int_score + 6
        elif disappearCnt == 4:
            self.int_score = self.int_score + 10




if __name__ == '__main__':
    app = QApplication(sys.argv)
    m = MainWnd()
    print("hello world")
    sys.exit(app.exec_())